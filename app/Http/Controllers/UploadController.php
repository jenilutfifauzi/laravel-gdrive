<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('thing');
        $filename = $file->getClientOriginalName();

        $dir = '/';
        $recursive = false;
        $contents = collect(Storage::disk('google')->listContents($dir, $recursive));
        $folder_name = 'folder_baru12';

        $dir = $contents->where('type', '=', 'dir')
            ->where('filename', '=', $folder_name)
            ->first();

        if (!$dir) {
            $dir = Storage::disk('google')->makeDirectory($folder_name);    
        }

        Storage::disk('google')->put($dir['path'] . '/' . $filename, file_get_contents($request->file('thing')->getRealPath()));

        return 'File was created in the sub directory in Google Drive';
    }
}
